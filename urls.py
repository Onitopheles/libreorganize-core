from django.conf.urls import url
from . import views

app_name = 'libreorganize'

urlpatterns = [
    url('^login/$', views.login, name='login'),
    url('^forums/create/', views.CreateView.as_view(), name='forum_create'),
]
