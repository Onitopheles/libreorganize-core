from django.shortcuts import render
from django.conf import settings
from tendenci.apps.theme.shortcuts import themed_response as render_to_resp
from tendenci.apps.forums.models import Forum
from tendenci.apps.accounts.forms import LoginForm
from django.views.generic import (
    CreateView
)
# Create your views here.

class ForumCreate(CreateView):
    model = Forum
    template_name = 'forum/forum_create.html'
    


def login(request, form_class=LoginForm, template_name="account/login.html"):

    redirect_to = request.GET.get('next', u'')

    if request.method == "POST":
        default_redirect_to = getattr(settings, "LOGIN_REDIRECT_URLNAME", None)
        if default_redirect_to:
            default_redirect_to = reverse(default_redirect_to)
        else:
            default_redirect_to = settings.LOGIN_REDIRECT_URL

        # light security check -- make sure redirect_to isn't garabage.
        if not redirect_to or "://" in redirect_to or " " in redirect_to:
            redirect_to = default_redirect_to

        form = form_class(request.POST, request=request)
        if form.login(request):
            EventLog.objects.log(instance=request.user, application="accounts")

            return HttpResponseRedirect(redirect_to)
    else:
        form = form_class(request=request)

        if request.user.is_authenticated and redirect_to:
            return HttpResponseRedirect(redirect_to)

    return render_to_resp(request=request, template_name=template_name, context={
        "form": form
    })
