from django.apps import AppConfig


class LibreorganizeConfig(AppConfig):
    name = 'libreorganize'
