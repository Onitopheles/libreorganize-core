from django import forms
from tendenci.apps.forums.models import Forum

class ForumForm(forms.ModelForm):
    class Meta:
        model = ForumForm
        fields = [
            'name',
            'description',
        ]